package com.exomind.exomindapp


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class SearchUserTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun searchUserTest() {
        val appCompatImageView = onView(
                allOf(
                        withId(R.id.search_button), withContentDescription("Rechercher"),
                        childAtPosition(
                                allOf(
                                        withId(R.id.search_bar),
                                        childAtPosition(
                                                withId(R.id.search),
                                                0
                                        )
                                ),
                                1
                        ),
                        isDisplayed()
                )
        )
        appCompatImageView.perform(click())

        val searchAutoComplete = onView(
                allOf(
                        withId(R.id.search_src_text),
                        childAtPosition(
                                allOf(
                                        withId(R.id.search_plate),
                                        childAtPosition(
                                                withId(R.id.search_edit_frame),
                                                1
                                        )
                                ),
                                0
                        ),
                        isDisplayed()
                )
        )
        searchAutoComplete.perform(replaceText("ervin"), closeSoftKeyboard())

        val textView = onView(
                allOf(
                        withId(R.id.name), withText("Ervin Howell"),
                        withParent(withParent(withId(R.id.userRv))),
                        isDisplayed()
                )
        )
        textView.check(matches(withText("Ervin Howell")))
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
