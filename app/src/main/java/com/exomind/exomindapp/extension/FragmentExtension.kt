package com.exomind.exomindapp.extension

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.exomind.exomindapp.utils.nav.NavigationCommand

/**
 * Needs to be implemented by fragments that should exit with circular reveal
 * animation along with [isToBeExitedWithAnimation] returning true
 * @property posX the X-axis position of the center of circular reveal
 * @property posY the Y-axis position of the center of circular reveal
 */
interface ExitWithAnimation {
    var posX: Int
    var posY: Int

    /**
     * Must return true if required to exit with circular reveal animation
     */
    fun isToBeExitedWithAnimation(): Boolean
}


/**
 * Handle generic navigation within the library
 *
 * @param navEvent : received navigation event
 * @param rootId : root navigation id
 */
fun Fragment.subscribeToNavigationEvents(navEvent: LiveData<NavigationCommand>, rootId: Int) {
    navEvent.observe(viewLifecycleOwner, Observer { command ->
        when (command) {
            is NavigationCommand.ToSafeArgs -> {
                findNavController().navigate(command.directions)
            }

            is NavigationCommand.To -> {
                findNavController().navigate(command.directions.actionId, command.bundle)
            }

            is NavigationCommand.Back -> {
                findNavController().navigateUp()
            }

            is NavigationCommand.BackTo -> {
                findNavController().popBackStack(command.destinationId, true)
            }

            is NavigationCommand.ToRoot -> {
                findNavController().popBackStack(rootId, true)
            }
        }
    })
}
