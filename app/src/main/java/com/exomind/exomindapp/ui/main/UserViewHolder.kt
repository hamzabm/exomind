package com.exomind.exomindapp.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.exomind.exomindapp.R
import com.exomind.exomindapp.data.model.User
import kotlinx.android.synthetic.main.ui_user_item.view.*

class UserViewHolder internal constructor(
        inflater: LayoutInflater,
        parent: ViewGroup
) : RecyclerView.ViewHolder(
        inflater.inflate(
                R.layout.ui_user_item,
                parent,
                false
        )
) {
    fun bind(user: User, listener: (item: User) -> Unit) = with(this.itemView) {

        itemView.setOnClickListener {
            listener(user)
        }
        name.text = user.name
        login.text = user.username
        mail.text = user.email
        tel.text = user.phone
        site.text = user.website
    }
}