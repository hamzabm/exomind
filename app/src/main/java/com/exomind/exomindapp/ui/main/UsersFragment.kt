package com.exomind.exomindapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.exomind.exomindapp.R
import com.exomind.exomindapp.extension.subscribeToNavigationEvents
import com.exomind.exomindapp.ui.base.AbstractBaseFragment
import kotlinx.android.synthetic.main.users_fragment.*
import org.koin.android.ext.android.inject

class UsersFragment : AbstractBaseFragment() {


    private val viewModel: UsersViewModel by inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.users_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.users_fragment_title)

    }


    override fun bindClicks() {
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.refreshListByFilterName(newText.toString())
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

        })
    }

    override fun setUi() {
        viewModel.usersList.observe(this, Observer {
            userRv.layoutManager = LinearLayoutManager(context)
            userRv.adapter = UsersAdapter(it) { user ->
                viewModel.goToUserAlbumFragment(user.id)
            }
        })
    }


    override fun subscribe() {
    }

    override fun subscribeToNavigationEvents() {
        subscribeToNavigationEvents(viewModel.navigationCommandsLiveEvent, R.id.nav_graph)
    }

}