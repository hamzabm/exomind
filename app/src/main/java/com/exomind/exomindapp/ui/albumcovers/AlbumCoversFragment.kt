package com.exomind.exomindapp.ui.albumcovers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.exomind.exomindapp.R
import com.exomind.exomindapp.extension.subscribeToNavigationEvents
import com.exomind.exomindapp.ui.base.AbstractBaseFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.album_covers_fragment.*
import org.koin.android.ext.android.inject

class AlbumCoversFragment : AbstractBaseFragment() {

    companion object {
        fun newInstance() = AlbumCoversFragment()
    }

    private val viewModel: AlbumCoversViewModel by inject()
    var userId = 0
    var albumId = 0
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        arguments?.let {

            userId = AlbumCoversFragmentArgs.fromBundle(it).userId.toInt()
            albumId = AlbumCoversFragmentArgs.fromBundle(it).albumId.toInt()

        }
        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        return inflater.inflate(R.layout.album_covers_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.loadAlbumCovers(userId, albumId)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.album_covers_frag_title)

    }

    override fun bindClicks() {

        viewModel.errors.observe(this, Observer { err ->
            err?.let {
                this.view?.let { view ->
                    Snackbar.make(view, it.message.toString(), Snackbar.LENGTH_LONG)

                            .setAction(getString(R.string.retry)) {
                                viewModel.loadAlbumCovers(userId, albumId)
                            }
                            .setDuration(10000)
                            .setActionTextColor(ContextCompat.getColor(requireContext(), android.R.color.holo_red_light))
                            .show()
                }
            }

        })
    }

    override fun setUi() {
        viewModel.albumCovers.observe(this, Observer {
            albumCoversRv.layoutManager = GridLayoutManager(context, 2)
            albumCoversRv.adapter = CoversAlbumAdapter(it)
        })
    }


    override fun subscribe() {
    }


    override fun subscribeToNavigationEvents() {
        subscribeToNavigationEvents(viewModel.navigationCommandsLiveEvent, R.id.nav_graph)
    }

}
