package com.exomind.exomindapp.ui.useralbums

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.exomind.exomindapp.R
import com.exomind.exomindapp.extension.subscribeToNavigationEvents
import com.exomind.exomindapp.ui.base.AbstractBaseFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.user_albums_fragment.*
import org.koin.android.ext.android.inject

class UserAlbumsFragment : AbstractBaseFragment() {

    companion object {
        fun newInstance() =
                UserAlbumsFragment()
    }

    private val viewModel: UserAlbumsViewModel by inject()
    private var userId: Long = 0

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        arguments?.let {

            userId = UserAlbumsFragmentArgs.fromBundle(it).userId
        }
        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)

        return inflater.inflate(R.layout.user_albums_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.user_albums_frag_title)
        viewModel.loadUserAlbums(userId.toInt())
    }

    override fun bindClicks() {
        viewModel.errors.observe(this, Observer { err ->
            err?.let {
                this.view?.let { view ->
                    Snackbar.make(view, it.message.toString(), Snackbar.LENGTH_LONG)
                            .setAction(getString(R.string.retry)) {
                                viewModel.loadUserAlbums(userId.toInt())
                            }
                            .setDuration(10000)
                            .setActionTextColor(ContextCompat.getColor(requireContext(), android.R.color.holo_red_light))
                            .show()
                }
            }

        })
    }

    override fun setUi() {
        viewModel.userAlbums.observe(this, Observer {
            userAlbumsRv.layoutManager = GridLayoutManager(context, 2)
            userAlbumsRv.adapter = UserAlbumsAdapter(it) {
                viewModel.goToAlbumCovers(userId, it.id)
            }
        })
    }


    override fun subscribe() {
    }

    override fun subscribeToNavigationEvents() {
        subscribeToNavigationEvents(viewModel.navigationCommandsLiveEvent, R.id.nav_graph)
    }

}