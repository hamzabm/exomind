package com.exomind.exomindapp.ui.useralbums

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.exomind.exomindapp.data.model.UserAlbum


class UserAlbumsAdapter internal constructor(
        private val userAlbums: ArrayList<UserAlbum>
        , val listener: (item: UserAlbum) -> Unit) :
        RecyclerView.Adapter<UserAlbumViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserAlbumViewHolder {
        return UserAlbumViewHolder(LayoutInflater.from(parent.context), parent)
    }

    override fun onBindViewHolder(holder: UserAlbumViewHolder, position: Int) {
        holder.bind(userAlbums[position], listener)
    }

    override fun getItemCount(): Int {
        return userAlbums.size
    }
}