package com.exomind.exomindapp.ui.useralbums

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.exomind.exomindapp.R
import com.exomind.exomindapp.data.model.UserAlbum
import kotlinx.android.synthetic.main.ui_user_album_item.view.*

class UserAlbumViewHolder internal constructor(
        inflater: LayoutInflater,
        parent: ViewGroup
) : RecyclerView.ViewHolder(
        inflater.inflate(
                R.layout.ui_user_album_item,
                parent,
                false
        )
) {
    fun bind(album: UserAlbum, listener: (item: UserAlbum) -> Unit) = with(this.itemView) {

        itemView.setOnClickListener {
            listener(album)
        }
        title.text = album.title

    }
}