package com.exomind.exomindapp.ui.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment

abstract class AbstractBaseFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUi()
        bindClicks()

        subscribe()
        subscribeToNavigationEvents()
    }

    abstract fun subscribe()
    abstract fun subscribeToNavigationEvents()
    abstract fun bindClicks()
    abstract fun setUi()


}