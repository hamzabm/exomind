package com.exomind.exomindapp.ui.albumcovers

import androidx.lifecycle.MutableLiveData
import com.exomind.exomindapp.data.local.dao.AlbumCoversDao
import com.exomind.exomindapp.data.model.AlbumCover
import com.exomind.exomindapp.data.model.CallResult
import com.exomind.exomindapp.data.service.JPServiceImpl
import com.exomind.exomindapp.ui.base.AbstractBaseViewModel
import com.exomind.exomindapp.ui.main.UsersFragmentDirections
import com.exomind.exomindapp.utils.nav.NavigationCommand
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AlbumCoversViewModel(private val jpServiceImpl: JPServiceImpl, private val albumCoversDao: AlbumCoversDao) : AbstractBaseViewModel() {

    internal val albumCovers = MutableLiveData<ArrayList<AlbumCover>>()

    fun loadAlbumCovers(userId: Int, albumId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            if (albumCoversDao.getByAlbumId(albumId).isNullOrEmpty()) {
                //loaded from server
                val result = withContext(Dispatchers.IO) {
                    jpServiceImpl.getAlbumCovers(userId, albumId)
                }
                when (result) {
                    is CallResult.Success -> {
                        albumCoversDao.insertAll(result.data)
                        albumCovers.postValue(result.data)
                    }
                    is CallResult.Error -> errors.postValue(result.exception)

                }

            } else {
                //loaded from data base
                albumCovers.postValue(ArrayList(albumCoversDao.getAll()))
            }
        }

    }


    fun goToUserAlbumFragment(userId: Int) {
        val action =
                UsersFragmentDirections.actionUsersFragmentToUserAlbumsFragment(userId.toLong())
        navigate(NavigationCommand.ToSafeArgs(action))
    }

    fun goBack() {
        navigate(NavigationCommand.Back)
    }
}