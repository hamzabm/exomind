package com.exomind.exomindapp.ui.albumcovers

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.exomind.exomindapp.R
import com.exomind.exomindapp.data.model.AlbumCover
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.ui_cover_album_item.view.*

class CoverAlbumViewHolder internal constructor(
        inflater: LayoutInflater,
        parent: ViewGroup
) : RecyclerView.ViewHolder(
        inflater.inflate(
                R.layout.ui_cover_album_item,
                parent,
                false
        )
) {
    fun bind(albumCover: AlbumCover) = with(this.itemView) {

        Picasso.get().load(albumCover.url).placeholder(R.drawable.place_holder).into(cover)

    }
}