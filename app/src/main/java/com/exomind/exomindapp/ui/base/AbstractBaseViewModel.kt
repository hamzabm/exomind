package com.exomind.exomindapp.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.exomind.exomindapp.utils.livedata.SingleLiveEvent
import com.exomind.exomindapp.utils.nav.NavigationCommand

abstract class AbstractBaseViewModel : ViewModel() {
    internal val errors = MutableLiveData<Exception>()


    // ===========================================================
    // Fields
    // ===========================================================
    /**
     * This one isn’t a bug, but it goes against separation of concerns.
     * Views - Fragments and Activities - shouldn’t be able of updating LiveData
     * and thus their own state because that’s the responsibility of ViewModels.
     *
     * ---> Views should be able to only observe LiveData. Values update are made by the viewModel on the MutableLiveData
     */
    private val navigationCommandsSLE: SingleLiveEvent<NavigationCommand> = SingleLiveEvent()
    val navigationCommandsLiveEvent: LiveData<NavigationCommand> = navigationCommandsSLE

    // ===========================================================
    // Public Methods
    // ===========================================================
    /**
     * Navigation helper method
     *
     * @param directions: destination to go to
     */
    fun navigate(command: NavigationCommand) {
        navigationCommandsSLE.postValue(command)
    }

}