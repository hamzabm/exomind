package com.exomind.exomindapp.ui.useralbums

import androidx.lifecycle.MutableLiveData
import com.exomind.exomindapp.data.local.dao.UserAlbumsDao
import com.exomind.exomindapp.data.model.CallResult
import com.exomind.exomindapp.data.model.UserAlbum
import com.exomind.exomindapp.data.service.JPServiceImpl
import com.exomind.exomindapp.ui.base.AbstractBaseViewModel
import com.exomind.exomindapp.utils.nav.NavigationCommand
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserAlbumsViewModel(private val jpServiceImpl: JPServiceImpl, private val userAlbumsDao: UserAlbumsDao) : AbstractBaseViewModel() {

    internal val userAlbums = MutableLiveData<ArrayList<UserAlbum>>()

    fun loadUserAlbums(userId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            if (userAlbumsDao.getByUserId(userId).isNullOrEmpty()) {
                //loaded from server
                val result = withContext(Dispatchers.IO) {
                    jpServiceImpl.getAlbumsByUserId(userId)
                }
                when (result) {
                    is CallResult.Success -> {
                        userAlbumsDao.insertAll(result.data)
                        userAlbums.postValue(result.data)
                    }
                    is CallResult.Error -> errors.postValue(result.exception)

                }

            } else {
                //loaded from data base
                userAlbums.postValue(ArrayList(userAlbumsDao.getAll()))
            }
        }

    }


    fun goToAlbumCovers(userId: Long, albumId: Int) {
        val action =
                UserAlbumsFragmentDirections.actionUserAlbumsFragmentToAlbumCoversFragment(userId, albumId.toLong())
        navigate(NavigationCommand.ToSafeArgs(action))
    }

    fun goBack() {
        navigate(NavigationCommand.Back)
    }

}
