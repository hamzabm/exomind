package com.exomind.exomindapp.ui.albumcovers

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.exomind.exomindapp.data.model.AlbumCover


class CoversAlbumAdapter internal constructor(
        private val AlbumCovers: ArrayList<AlbumCover>) :
        RecyclerView.Adapter<CoverAlbumViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoverAlbumViewHolder {
        return CoverAlbumViewHolder(LayoutInflater.from(parent.context), parent)
    }

    override fun onBindViewHolder(holder: CoverAlbumViewHolder, position: Int) {
        holder.bind(AlbumCovers[position])
    }

    override fun getItemCount(): Int {
        return AlbumCovers.size
    }
}