package com.exomind.exomindapp.ui.main

import androidx.lifecycle.MutableLiveData
import com.exomind.exomindapp.data.local.dao.UserDao
import com.exomind.exomindapp.data.model.CallResult
import com.exomind.exomindapp.data.model.User
import com.exomind.exomindapp.data.service.JPServiceImpl
import com.exomind.exomindapp.ui.base.AbstractBaseViewModel
import com.exomind.exomindapp.utils.nav.NavigationCommand
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UsersViewModel(private val jpServiceImpl: JPServiceImpl, private val userDao: UserDao) : AbstractBaseViewModel() {

    internal val usersList = MutableLiveData<ArrayList<User>>()

    init {
        CoroutineScope(Dispatchers.IO).launch {
            if (userDao.getAll().isNullOrEmpty()) {
                //loaded from server
                val result = withContext(Dispatchers.IO) {
                    jpServiceImpl.getUsers()
                }
                when (result) {
                    is CallResult.Success -> {
                        userDao.insertAll(result.data)
                        usersList.postValue(result.data)
                    }
                    is CallResult.Error -> errors.postValue(result.exception)

                }

            } else {
                //loaded from data base
                usersList.postValue(ArrayList(userDao.getAll()))
            }
        }

    }

    fun refreshListByFilterName(query: String) {
        CoroutineScope(Dispatchers.IO).launch {
            usersList.postValue(ArrayList(userDao.filterByName("%$query%")))
        }
    }


    fun goToUserAlbumFragment(userId: Int) {
        val action = UsersFragmentDirections.actionUsersFragmentToUserAlbumsFragment(userId.toLong())
        navigate(NavigationCommand.ToSafeArgs(action))
    }
}