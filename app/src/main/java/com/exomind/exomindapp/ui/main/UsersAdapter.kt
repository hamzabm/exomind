package com.exomind.exomindapp.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.exomind.exomindapp.data.model.User


class UsersAdapter internal constructor(
        private val users: ArrayList<User>
        , val listener: (item: User) -> Unit) :
        RecyclerView.Adapter<UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(LayoutInflater.from(parent.context), parent)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(users[position], listener)
    }

    override fun getItemCount(): Int {
        return users.size
    }
}