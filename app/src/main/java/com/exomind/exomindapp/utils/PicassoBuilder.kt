package com.exomind.exomindapp.utils

import android.content.Context
import com.squareup.picasso.LruCache
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.Cache
import okhttp3.OkHttpClient
import java.io.File
import java.util.concurrent.TimeUnit

object PicassoBuilder {

    @Synchronized
    fun build(context: Context): Picasso {
        //for offline
        val cacheSize = 100 * 1024 * 1024 // 100 MiB
        val cacheDirectory = File(context.applicationContext.cacheDir.absolutePath, "picasso")

        val okHttpClient = OkHttpClient.Builder()
        okHttpClient.connectTimeout(30, TimeUnit.SECONDS)
        okHttpClient.readTimeout(60, TimeUnit.SECONDS)
        okHttpClient.retryOnConnectionFailure(true)

        cacheDirectory.mkdirs()
        val cache = Cache(cacheDirectory, cacheSize.toLong())
        okHttpClient.cache(cache)

        val instance = Picasso.Builder(context.applicationContext).downloader(OkHttp3Downloader(okHttpClient.build()))
                .memoryCache(LruCache(1024 * 1024 * 100))
                .build()
//        instance.setIndicatorsEnabled(BuildConfig.DEBUG)
        Picasso.setSingletonInstance(instance)
        return instance

    }
}