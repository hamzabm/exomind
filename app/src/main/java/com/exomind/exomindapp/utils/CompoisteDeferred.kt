package com.exomind.exomindapp.utils

import kotlinx.coroutines.Deferred

class CompositeDeferred {
    private val jobs: ArrayList<Deferred<Unit>> = ArrayList()

    fun add(items: Deferred<Unit>) {
        jobs.add(items)
    }

    fun cancel() {
        for (job in this.jobs) {
            job.cancel()
        }
        jobs.clear()
    }
}