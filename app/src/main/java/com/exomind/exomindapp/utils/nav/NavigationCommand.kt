package com.exomind.exomindapp.utils.nav

import android.os.Bundle
import androidx.navigation.NavDirections

/**
 * We use the command pattern to communicate between ViewModel and Fragment.
 * These are the commands that we use for navigation
 *
 * See FragmentExtension.kt for usage
 *
 */
sealed class NavigationCommand {
    data class ToSafeArgs(val directions: NavDirections) : NavigationCommand()
    data class To(val directions: NavDirections, val bundle: Bundle? = null) : NavigationCommand()
    object Back : NavigationCommand()
    data class BackTo(val destinationId: Int) : NavigationCommand()
    object ToRoot : NavigationCommand()
}