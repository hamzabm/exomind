package com.exomind.exomindapp

import android.app.Application
import com.exomind.exomindapp.di.appModule
import com.exomind.exomindapp.di.localDataSourcesModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class ExomindApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoinModule()
    }

    private fun initKoinModule() {
        startKoin {
            androidLogger()
            androidContext(this@ExomindApplication)
            modules(listOf(appModule,
                    localDataSourcesModules))
        }
    }

}
