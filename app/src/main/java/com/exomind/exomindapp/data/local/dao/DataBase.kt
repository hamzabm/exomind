package com.exomind.exomindapp.data.local.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.exomind.exomindapp.data.model.AlbumCover
import com.exomind.exomindapp.data.model.User
import com.exomind.exomindapp.data.model.UserAlbum

@Database(
        entities = [
            User::class,
            AlbumCover::class,
            UserAlbum::class
        ], version = 3
)

abstract class Database : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun albumCoversDao(): AlbumCoversDao
    abstract fun userAlbumsDao(): UserAlbumsDao
}