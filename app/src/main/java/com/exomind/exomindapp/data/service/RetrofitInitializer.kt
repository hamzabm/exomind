package com.exomind.exomindapp.data.service

import retrofit2.Retrofit

class RetrofitInitializer(private val retrofit: Retrofit) {
    fun jpService(): JPService = retrofit.create(JPService::class.java)
}