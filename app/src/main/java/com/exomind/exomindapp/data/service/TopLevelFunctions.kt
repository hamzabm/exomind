package com.exomind.exomindapp.data.service

import com.exomind.exomindapp.data.model.CallResult

object TopLevelFunctions {
    suspend fun <T : Any> safeApiCall(call: suspend () -> CallResult<T>, errorMessage: String): CallResult<T> = try {
        call.invoke()
    } catch (e: Exception) {
        CallResult.Error(e)
    }
}