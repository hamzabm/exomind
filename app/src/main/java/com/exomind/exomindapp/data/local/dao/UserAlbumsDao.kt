package com.exomind.exomindapp.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.exomind.exomindapp.data.model.UserAlbum

@Dao
interface UserAlbumsDao : BaseDao<UserAlbum> {
    @Query("SELECT * FROM UserAlbum")
    fun getAll(): List<UserAlbum>

    @Query("SELECT * FROM UserAlbum where userId==:idUser")
    fun getByUserId(idUser: Int): List<UserAlbum>


    @Query("DELETE FROM UserAlbum")
    fun clearTable()
}