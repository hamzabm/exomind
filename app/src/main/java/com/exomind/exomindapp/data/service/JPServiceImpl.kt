package com.exomind.exomindapp.data.service

import com.exomind.exomindapp.data.model.AlbumCovers
import com.exomind.exomindapp.data.model.CallResult
import com.exomind.exomindapp.data.model.UserAlbums
import com.exomind.exomindapp.data.model.Users
import com.exomind.exomindapp.data.service.TopLevelFunctions.safeApiCall
import java.io.IOException

class JPServiceImpl(private val jpService: JPService) {

    companion object {
        const val genericErrorMessage: String = "Error occurred"
    }

    suspend fun getAlbumCovers(userId: Int, albumId: Int) = safeApiCall(
            call = {
                val response = jpService.getAlbumCovers(userId, albumId).await()
                return@safeApiCall if (response.isSuccessful) {
                    CallResult.Success(response.body() as AlbumCovers)
                } else {
                    CallResult.Error(IOException("Error occurred during getAlbumCovers !"))
                }
            },
            errorMessage = genericErrorMessage
    )

    suspend fun getUsers() = safeApiCall(
            call = {
                val response = jpService.getUsers().await()
                return@safeApiCall if (response.isSuccessful) {
                    CallResult.Success(response.body() as Users)
                } else {
                    CallResult.Error(IOException("Error occurred during getUsers !"))
                }
            },
            errorMessage = genericErrorMessage
    )

    suspend fun getAlbumsByUserId(userId: Int) = safeApiCall(
            call = {
                val response = jpService.getAlbumsByUserId(userId).await()
                return@safeApiCall if (response.isSuccessful) {
                    CallResult.Success(response.body() as UserAlbums)
                } else {
                    CallResult.Error(IOException("Error occurred during getAlbumsByUserId !"))
                }
            },
            errorMessage = genericErrorMessage
    )

}