package com.exomind.exomindapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AlbumCover(
        var albumId: Int,
        @PrimaryKey var id: Int,
        var thumbnailUrl: String,
        var title: String,
        var url: String
)