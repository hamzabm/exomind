package com.exomind.exomindapp.data.model

data class Geo(
        var lat: String,
        var lng: String
)