package com.exomind.exomindapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserAlbum(
        @PrimaryKey var id: Int,
        var title: String,
        var userId: Int
)