package com.exomind.exomindapp.data.service

import com.exomind.exomindapp.data.model.AlbumCovers
import com.exomind.exomindapp.data.model.UserAlbums
import com.exomind.exomindapp.data.model.Users
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface JPService {

    @GET("users/{user_id}/photos")
    fun getAlbumCovers(
            @Path("user_id") userId: Int,
            @Query("albumId") albumId: Int
    ): Deferred<Response<AlbumCovers>>

    @GET("users")
    fun getUsers(): Deferred<Response<Users>>

    @GET("/users/{user_id}/albums")
    fun getAlbumsByUserId(
            @Path("user_id") userId: Int
    ): Deferred<Response<UserAlbums>>
}