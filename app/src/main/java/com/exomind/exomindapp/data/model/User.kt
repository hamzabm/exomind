package com.exomind.exomindapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
        var email: String,
        @PrimaryKey var id: Int,
        var name: String,
        var phone: String,
        var username: String,
        var website: String
)