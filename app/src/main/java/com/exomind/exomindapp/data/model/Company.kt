package com.exomind.exomindapp.data.model

data class Company(
        var bs: String,
        var catchPhrase: String,
        var name: String
)