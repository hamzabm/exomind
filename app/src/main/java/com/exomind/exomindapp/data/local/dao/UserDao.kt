package com.exomind.exomindapp.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.exomind.exomindapp.data.model.User

@Dao
interface UserDao : BaseDao<User> {
    @Query("SELECT * FROM User")
    fun getAll(): List<User>

    @Query("SELECT * FROM User where name like :query ")
    fun filterByName(query: String): List<User>

    @Query("SELECT * FROM User where id==:idUser")
    fun getById(idUser: Int): User


    @Query("DELETE FROM User")
    fun clearTable()
}