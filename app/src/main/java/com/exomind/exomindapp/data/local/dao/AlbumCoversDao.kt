package com.exomind.exomindapp.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.exomind.exomindapp.data.model.AlbumCover

@Dao
interface AlbumCoversDao : BaseDao<AlbumCover> {
    @Query("SELECT * FROM AlbumCover")
    fun getAll(): List<AlbumCover>

    @Query("SELECT * FROM AlbumCover where id==:idAlbum")
    fun getByAlbumId(idAlbum: Int): List<AlbumCover>


    @Query("DELETE FROM AlbumCover")
    fun clearTable()
}