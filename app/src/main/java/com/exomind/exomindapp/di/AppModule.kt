package com.exomind.exomindapp.di


import com.exomind.exomindapp.ui.albumcovers.AlbumCoversViewModel
import com.exomind.exomindapp.ui.main.UsersViewModel
import com.exomind.exomindapp.ui.useralbums.UserAlbumsViewModel
import com.exomind.exomindapp.utils.PicassoBuilder
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { UsersViewModel(get(), get()) }
    viewModel { AlbumCoversViewModel(get(), get()) }
    viewModel { UserAlbumsViewModel(get(), get()) }
    single { PicassoBuilder.build(get()) }

}
