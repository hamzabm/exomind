package com.exomind.exomindapp.di

import androidx.room.Room
import com.exomind.exomindapp.data.local.dao.Database
import com.exomind.exomindapp.data.service.JPServiceImpl
import com.exomind.exomindapp.data.service.RetrofitInitializer
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Koin main module
 */
val localDataSourcesModules = module {
    single { provideInterceptedHttpClient() }
    single { provideRetrofit(get()) }
    factory { RetrofitInitializer(get()).jpService() }
    factory { JPServiceImpl(get()) }

    single(definition = {
        Room.databaseBuilder(androidContext(), Database::class.java, "exomind")
                .build()
    })
    single(definition = { get<Database>().albumCoversDao() })
    single(definition = { get<Database>().userAlbumsDao() })
    single(definition = { get<Database>().userDao() })
}


fun provideInterceptedHttpClient(): OkHttpClient {

    return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .build()
}

var baseUrl = "https://jsonplaceholder.typicode.com/"
fun provideRetrofit(client: OkHttpClient): Retrofit {
    return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
}